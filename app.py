from bottle import route, run, get, post, request, redirect, static_file
import bottle as b
import pytz
import datetime
import os

app = b.default_app()

@route('/')
def basicroute():
	redirect('https://theslytherin.gitlab.io/c17e')

@route('/sidechannel')
def basic():
	return("Rahul Jha")



@post("/createcomment/")
def create():
	author = request.forms.get("fields[username]")
	comment = request.forms.get("fields[comment]")
	slug = request.forms.get("options[slug]")
	redir = request.forms.get("options[redirect]")
	app = request.forms.get("options[projID]")
	tz_NY = pytz.timezone('Asia/Kolkata')
	today = datetime.datetime.now(tz_NY)
	date = today.strftime('%H-%M-%S-%Y-%m-%d')
	markdown_text = markdownify_comment(author, comment, date)
	status = commit_to_gitlab(markdown_text, slug, author, date, app)
	redirect(redir)

@post("/createpost/")
def create_post():
	title = request.forms.get("fields[title]")
	author = request.forms.get("fields[username]")
	comment = request.forms.get("fields[comment]")
	tags = request.forms.get("fields[tags]")
	tz_NY = pytz.timezone('Asia/Kolkata')
	today = datetime.datetime.now(tz_NY)
	date_ = today.strftime('%H-%M-%S-%Y-%m-%d')
	slug = "{0}.html".format(date_)
	redir = request.forms.get("options[redirect]")
	app = request.forms.get("options[projID]")
	date = today.strftime('%H-%M-%S-%Y-%m-%d')
	markdown_text = markdownify(title, author, comment, date, slug, tags)
	status = commit_post_to_gitlab(markdown_text, slug, author, date, app, date_)
	redirect(redir)

def markdownify(title, author, comment, date,slug, tags):
	text = "title: {3}\nauthor : {0}\ndate: {1} \nurl: {4}\ntags: {5}\n\n {2}".format(author, date, comment, title, slug, tags)
	print(text)
	#print(text)
	return text

def markdownify_comment(author, comment, date):
	text = "author : {0}\ndate: {1} \n\n {2}".format(author, date, comment)
	return text

def commit_to_gitlab(content, slug, author, date, app):
	import json
	import random

	filename = random.randint(23112,243232412)
	payload = {
	    "branch" : "main",
	    "commit_message" : "Comment by {0}".format(author),
	    "actions":[
	    {
	        "action": "create",
	        "file_path": "_comments/{0}/{1}.md".format(slug, filename),
	        "content" : content
	    }
	    ]
	}
	headers = {'Content-type': 'application/json', "PRIVATE-TOKEN": os.environ.get('GITLAB_TOKEN')}
	import requests
	url = "https://gitlab.com/api/v4/projects/{0}/repository/commits".format(app)
	r = requests.post(url, data = json.dumps(payload), headers = headers)
	k = r.json()
	if 'id' in k:
	    return True
	else:
	    return False

def commit_post_to_gitlab(content, slug, author, date, app, date_):
	import json
	import random

	filename = date_
	payload = {
	    "branch" : "main",
	    "commit_message" : "New question by {0}".format(author),
	    "actions":[
	    {
	        "action": "create",
	        "file_path": "_posts/{0}.md".format(filename),
	        "content" : content
	    }
	    ]
	}
	headers = {'Content-type': 'application/json', "PRIVATE-TOKEN": os.environ.get('GITLAB_TOKEN')}
	import requests
	url = "https://gitlab.com/api/v4/projects/{0}/repository/commits".format(app)
	r = requests.post(url, data = json.dumps(payload), headers = headers)
	k = r.json()
	print(k)
	if 'id' in k:
	    return True
	
	else:
	    return False



if os.environ.get('APP_LOCATION') == 'heroku':
    run(host="0.0.0.0", port=int(os.environ.get("PORT", 5000)), debug = True )
else:
    run(host='localhost', port=5000, debug = True)
